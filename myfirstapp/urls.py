from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('aboutus/', views.aboutus, name="aboutus"), # '' + / + aboutus/ => /aboutus/
    path('training/', views.training, name="training"),
    path('blog/', views.blog, name="blog"),
    path('contactus/', views.contactus, name="contactus")
]