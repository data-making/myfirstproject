from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def home(request):
    #return HttpResponse("Hello Django Application!!!")
    return render(request, "home.html")

def aboutus(request):
    #return HttpResponse("Please navigate to About Us page")
    return render(request, "aboutus.html")

def training(request):
    return render(request, "training.html")

def blog(request):
    return render(request, "blog.html")

def contactus(request):
    return render(request, "contactus.html")